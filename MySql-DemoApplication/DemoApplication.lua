local configEnv = {} -- to keep it separate from the global env
local f,err = loadfile("config.txt", "t", configEnv)
if f then
   f() -- run the chunk
else
   print(err)
end

-- Request Method
wrk.method = "GET"

-- Request Headers
wrk.headers["accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
wrk.headers["accept-encoding"] = "gzip, deflate"
wrk.headers["accept-language"] = "en-US,en;q=0.9,hi;q=0.8"
wrk.headers["cache-control"] = "no-cache"
wrk.headers["connection"] = "keep-alive"
wrk.headers["cookie"] = "JSESSIONID=7B3C9B8549877404A802AC3AD7F03B20"
wrk.headers["referer"] = "http://" .. configEnv.target.ip .. ":" .. configEnv.target.port .. "/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp"
wrk.headers["upgrade-insecure-requests"] = "1"
wrk.headers["user-agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"
