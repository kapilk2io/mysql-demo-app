package com.k2cybersecurity.demo;

import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class LocalConnection {

	private static java.sql.Connection conn;
	private static int TIMEOUT = 3000; 
	
	
	private static void createConnection() {
		InputStream input = null;
		String userName = "";
		String password = "";
		String dbName = "";
		String host = "";
		String port = "";
		Properties prop = new Properties();
		String filename = "db.properties";
		try {
			input = LocalConnection.class.getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				System.out.println("Sorry, unable to find " + filename);
				return;
			}

			// load a properties file from class path, inside static method
			prop.load(input);

			// // get the property value and print it out
			host = prop.getProperty("mysql.host");
			port = prop.getProperty("mysql.port");
			userName = prop.getProperty("mysql.username");
			password = prop.getProperty("mysql.password");
			dbName = prop.getProperty("mysql.dbname");

			// get the property value and print it out mssql
			// host = prop.getProperty("mssql.host");
			// port = prop.getProperty("mssql.port");
			// userName = prop.getProperty("mssql.username");
			// password = prop.getProperty("mssql.password");
			// dbName = prop.getProperty("mssql.dbname");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// String url = "jdbc:sqlserver://" + host + ":" + port +
		// ";databaseName=" + dbName;
		String url = "jdbc:mysql://" + host + ":" + port + "/";
		String driver = "com.mysql.jdbc.Driver";

		try {
			Class.forName(driver).newInstance();
			// Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
			setConnection(DriverManager.getConnection(url + dbName, userName, password));
			System.out.println("Connected to the database");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static java.sql.Connection getConnection() throws SQLException {
		if (LocalConnection.conn==null || !LocalConnection.conn.isValid(TIMEOUT)){
			createConnection();
		}
		return LocalConnection.conn;
	}

	public static void setConnection(java.sql.Connection conn) {
		LocalConnection.conn = conn;
	}

}
