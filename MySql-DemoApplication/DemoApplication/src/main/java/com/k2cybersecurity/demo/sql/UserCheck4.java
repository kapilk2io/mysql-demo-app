package com.k2cybersecurity.demo.sql;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.PreparedStatement;



@WebServlet(name = "UserCheck4", urlPatterns = { "/UserCheck4" })
public class UserCheck4 extends HttpServlet {
	
	@FunctionalInterface
	public interface ResultSetProcessor {

	    public void process(ResultSet resultSet, 
	                        long currentRow) 
	                        throws SQLException;

	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8849927226051145843L;
	public static Connection conn = null;
	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {

			out.println("<html>");
			out.println("<head>");
			out.println("<title>Users Data</title>");
			out.println("</head>");
			out.println(
					"<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>"
							+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl' crossorigin='anonymous'></script>");
			out.println("<body>");
			String user = request.getParameter("user");
			String pwd = request.getParameter("password");
			System.out.println("MySQL Connect Example.");
			
			Properties prop = new Properties();
			InputStream input = null;
			String userName = "";
			String password = "";
			String dbName = "";
			String host = "";
			String port = "";
			try {

				String filename = "db.properties";
				input = UserCheck.class.getClassLoader().getResourceAsStream(filename);
				if (input == null) {
					System.out.println("Sorry, unable to find " + filename);
					return;
				}

				// load a properties file from class path, inside static method
				prop.load(input);

				// get the property value and print it out
				host = prop.getProperty("mysql.host");
				port = prop.getProperty("mysql.port");
				userName = prop.getProperty("mysql.username");
				password = prop.getProperty("mysql.password");
				dbName = prop.getProperty("mysql.dbname");

			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			String url = "jdbc:mysql://" + host + ":" + port + "/";

			String driver = "com.mysql.jdbc.Driver";

			try {
				Class.forName(driver).newInstance();
				conn = DriverManager.getConnection(url + dbName + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&autoReconnect=true&useSSL=false", userName, password);
				System.out.println("Connected to the database");
				conn.setAutoCommit(false);
				Statement st = conn.createStatement();

				// get user name list
//				String tempQuery = "SELECT userId FROM  User";
				out.print("<a href='index.jsp' class='btn btn-warning'>Exit</a>");
				out.println("<br><br>Following users were added<br>");
				int i=0;
				for(i=0;i<10; i++) {
					int leftLimit = 97; // letter 'a'
				    int rightLimit = 122; // letter 'z'
				    int targetStringLength = 10;
				    Random random = new Random();
				    StringBuilder buffer = new StringBuilder(targetStringLength);
				    for (int i1 = 0; i1 < 7; i1++) {
				        int randomLimitedInt = leftLimit + (int) 
				          (random.nextFloat() * (rightLimit - leftLimit + 1));
				        buffer.append((char) randomLimitedInt);
				    }
				    String username = buffer.toString();
				    String pass=username;
					String query="INSERT INTO User (userId,password,city,country,email,phone,firstName,lastName) VALUES ('" 
								+ username	+"','" + pass +"','city7','India','"+username+"@abc.in','333333333','"+username+"','temp')";
					
					out.println("'"+ username	+"','" + pass +"','city7','India','"+username+"@abc.in','333333333','"+username+"','temp'");
					out.println("<br>");
					st.addBatch(query);
				}
					
				int[] count = st.executeBatch();
				conn.commit();
//					select(conn, query,(rs1, cnt)-> {   
//						System.out.println("in lambda expression!!!");
//					 	System.out.println(rs1.getString("city")+" "+cnt);
//					 	
////					 	if (!rs1.next())
////							out.println("<div class='alert-danger'>Invalid Login Credentials</div>");
////						else {
//							//rs1.beforeFirst();
//							out.println("<div class='table-responsive'>");
//							out.println("<table class='table'>");
//							out.println("<tr><td>Hello, " + user + " your details are as follows:</td></tr>");
//							//System.out.println();
//							
//	
//								out.println("<td>"+rs1.getString("city")+"</td>");
//								out.println("<td>"+rs1.getString("country")+"</td>");
//								out.println("<td>"+rs1.getString("email")+"</td>");
//								out.println("<td>"+rs1.getString("phone")+"</td>");
//								out.println("<td>"+rs1.getString("firstName")+"</td>");
//								out.println("<td>"+rs1.getString("lastName")+"</td>");
//	
//							
//							out.println("</table></div>");
//						//}
//					});
//					
					conn.close();
					System.out.println("Disconnected from database");
				
			} catch (Exception e) {
				e.printStackTrace();
			}

			out.println("</body>");
			out.println("</html>");

		}

		finally {
			out.close();
		}
	}

	public static void select(Connection connection, 
            String sql, 
            ResultSetProcessor processor, 
            Object... params) {
try (java.sql.PreparedStatement ps = connection.prepareStatement(sql)) {
int cnt = 0;
for (Object param : params) {
	System.out.println("param " + param);
  ps.setObject(++cnt, param);
}
try (ResultSet rs = ps.executeQuery()) {
  long rowCnt = 0;
  while (rs.next()) {
	  System.out.println("rs " + rs);
      processor.process(rs, rowCnt++);
  }
} catch (SQLException e) {
  System.out.println(e);
}
} catch (SQLException e) {
	System.out.println(e);
}
}
	
	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>
}