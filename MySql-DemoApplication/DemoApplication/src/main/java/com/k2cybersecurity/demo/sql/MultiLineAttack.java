package com.k2cybersecurity.demo.sql;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet(name = "MultiLineAttack", urlPatterns = { "/MultiLineAttack" })
public class MultiLineAttack extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8849927226051145843L;

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {

			out.println("<html>");
			out.println("<head>");
			out.println("<title>Users Data</title>");
			out.println("</head>");
			out.println(
					"<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>"
							+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl' crossorigin='anonymous'></script>");
			out.println("<body>");
			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");

String midname= request.getParameter("midname");		
System.out.println("MySQL Connect Example.");
			Connection conn = null;
			Properties prop = new Properties();
			InputStream input = null;
			String userName = "";
			String password = "";
			String dbName = "";
			String host = "";
			String port = "";
			try {

				String filename = "db.properties";
				input = UserCheck.class.getClassLoader().getResourceAsStream(filename);
				if (input == null) {
					System.out.println("Sorry, unable to find " + filename);
					return;
				}

				// load a properties file from class path, inside static method
				prop.load(input);

				// get the property value and print it out
				host = prop.getProperty("mysql.host");
				port = prop.getProperty("mysql.port");
				userName = prop.getProperty("mysql.username");
				password = prop.getProperty("mysql.password");
				dbName = prop.getProperty("mysql.dbname");

			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			String url = "jdbc:mysql://" + host + ":" + port + "/";

			String driver = "com.mysql.jdbc.Driver";

			try {
				Class.forName(driver).newInstance();
				conn = DriverManager.getConnection(url + dbName + "?allowMultiQueries=true", userName, password);
				System.out.println("Connected to the database");

				Statement st = conn.createStatement();

				// get user name list
//				String tempQuery = "SELECT userId FROM  User";
				out.print("<a href='index.jsp' class='btn btn-warning'>Exit</a>");
				ResultSet rs = st.executeQuery("SELECT userId FROM  User");
				boolean userExists = false;
				
				
					String query = "SELECT city, country, email, phone, firstname, lastname FROM  User where userid='"
							+ firstname+midname+lastname + "'";

					// PreparedStatement preparedStatement=conn.prepareStatement("SELECT * FROM
					// usercheck where username=?") ;
					// preparedStatement.setString(1, user);

					ResultSet resultSet = st.executeQuery(query);
					// ResultSet res = preparedStatement.executeQuery();
					if (!resultSet.next())
						out.println("<div class='alert-danger'>Invalid Login Credentials</div>");
					else {
						resultSet.beforeFirst();
						ResultSetMetaData rsmd = resultSet.getMetaData();
						int columnsNumber = rsmd.getColumnCount();
						String columnName[] = new String[columnsNumber];
						out.println("<div class='table-responsive'>");
						out.println("<table class='table'>");
						out.println("<tr><td>Hello, " + firstname+lastname + " your details are as follows:</td></tr>");
						while (resultSet.next()) {

							for (int i = 1; i <= columnsNumber; i++) {
								out.println("<tr>");
								columnName[i - 1] = rsmd.getColumnLabel(i);
								out.println("<td class='table-info' style='width:40%'>");
								out.println(columnName[i - 1]);
								out.println("</td>");
								out.println("<td style='width:60%'>");
								out.print(resultSet.getString(i));
								out.println("</td>");
								out.println("</tr>");
							}

						}
						out.println("</table></div>");
					}
					conn.close();
					System.out.println("Disconnected from database");
				
			} catch (Exception e) {
				e.printStackTrace();
			}

			out.println("</body>");
			out.println("</html>");

		}

		finally {
			out.close();
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>
}

