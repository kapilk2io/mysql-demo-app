package com.k2cybersecurity.demo.stringbuilder;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.k2cybersecurity.demo.LocalConnection;

@WebServlet(name = "SBCASE14", urlPatterns = { "/SBCase14" })
public class Case14 extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8849927226051145843L;

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {

			out.println("<html>");
			out.println("<head>");
			out.println("<title>Users Data</title>");
			out.println("</head>");
			out.println(
					"<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>"
							+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl' crossorigin='anonymous'></script>");
			out.println("<body>");
			String user = request.getParameter("user");
			String pwd = request.getParameter("password");
			
			try {
				out.print("<a href='index.jsp' class='btn btn-warning'>Exit</a>");
				Connection conn = LocalConnection.getConnection();
				

				Statement st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				String str1 = "select * from User where userid='";
				String str2 = "'";
				String str3 = " and password='";
				
				StringBuilder sb = new StringBuilder(str1);
				StringBuilder sb2 = new StringBuilder(str3);
				
				String regex = "^[rR][oO][oO][tT]$";
				
				if(user==null){
					user = new String("root");
					sb.append(user);
					sb.append(str2);
					if (pwd.isEmpty()) {
						
						pwd = "someSecretPasswordUnderTheHood";
						
					}
					sb.append(sb2);
					sb.append(pwd);
					sb.append(str2);
					
				} else if(!user.isEmpty()){
					sb.append(user);
					sb.append(str2);
					if (Pattern.matches(regex, user)){
						if (pwd.isEmpty()) {
							sb.append(sb2);
							sb.append("someSecretPasswordUnderTheHood");
							sb.append(str2);
						} 
					} else {
						if (pwd.isEmpty()) {
							pwd = "";
						}
						sb.append(sb2);
						sb.append(pwd);
						sb.append(str2);
					}
					
				}
				else {
					sb.append(user);
					sb.append(str2);
					sb.append(sb2);
					sb.append(pwd);
					sb.append(str2);
				}
				
				
				
				ResultSet resultSet = st.executeQuery(sb.toString());
				
				String result = "<table>";

				ResultSetMetaData rsmd = resultSet.getMetaData();
				int columnsNumber = rsmd.getColumnCount();
				String columnName[] = new String[columnsNumber];
				result += "<div class='table-responsive'>";
				result += "<table class='table'><tr class='table-info'>";
				for (int i = 1; i <= columnsNumber; i++) {
					columnName[i - 1] = rsmd.getColumnLabel(i);
					result += "<td>";
					result += columnName[i - 1];
					result += "</td>";
				}
				result += "</tr>";

				while (resultSet.next()) {

					result += "<tr>";
					for (int i = 1; i <= columnsNumber; i++) {
						result += "<td>";
						result += resultSet.getString(i);
						result += "</td>";
					}
					result += "</tr>";

				}
				result += "</table>";
				out.println(result);
			
				

			} catch (Exception e) {
				e.printStackTrace();
			}

			out.println("</body>");
			out.println("</html>");

		}

		finally {
			out.close();
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on
	// the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}