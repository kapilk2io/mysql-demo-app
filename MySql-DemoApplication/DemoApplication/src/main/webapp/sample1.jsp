
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
</head>
<body>
	<h1 class="alert text-secondary" >User Login</h1>
	<a href="index.jsp" class="btn btn-warning">Back</a>
	<form action="UserCheck">
		<div class="form-group">
			<label for="user" class="text-secondary"  style="color: #60F11D">User Name:</label> <input type="text"
				class="form-control" id="user" name="user">
		</div>
		<div class="form-group">
			<label for="password" class="text-secondary" style="color: #60F11D">Password:</label> <input type="text"
				class="form-control" id="password" name="password">
		</div>
		<input type="submit" class="btn btn-success" value="Submit" />
	</form>
</body>
</html>