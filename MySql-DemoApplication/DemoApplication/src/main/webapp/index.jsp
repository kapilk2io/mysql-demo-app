<%-- 
    Document   : index
    Created on : Nov 3, 2011, 4:33:22 PM
    Author     : ramakrishnan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
</head>

<body>
		<ul class="list-group">
		<li class="list-group-item active" style="width: 100%">Choose your role!!</li>
		<li class="list-group-item" >
			<a href="sample1.jsp" class="btn btn-secondary">User</a>
		</li>
		<li class="list-group-item" >
			<a href="sample2.jsp" class="btn btn-secondary" style="margin: auto;">Admin</a>
		</li>
		<li class="list-group-item" >
			<a href="sample3.jsp" class="btn btn-secondary" style="margin: auto;">multi line query with comments</a>
		</li>
		<li class="list-group-item" >
			<a href="sample4.jsp" class="btn btn-secondary" style="margin: auto;">Batch</a>
		</li>
		<li class="list-group-item" >
			<a href="sample6.jsp" class="btn btn-secondary" style="margin: auto;">DATE</a>
		</li>
                <li class="list-group-item" >
			<a href="sample7.jsp" class="btn btn-secondary" style="margin: auto;">For split attack Payload  in multiple lines</a>
		</li>		
	</ul>
</body>
</html>
