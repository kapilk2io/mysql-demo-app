#!/bin/bash

#start mysql
mysqld_safe &
echo "Sleeping 30 sec for MySql startup..."
sleep 30
#Populate java application DemoApplication
cd /
mysql -u root --execute="create database test"
mysql -u root test < MySql-DemoApplication-db.sql

#start tomcat
export CATALINA_OPTS=$K2_OPTS
/bin/sh /etc/apache-tomcat/bin/catalina.sh run &
cd /
echo "Sleeping 30 sec for Tomcat startup..."
sleep 30
