#!/bin/bash


# Attach Javaagent Statically
sed -i  's+genericJvmArguments="">+genericJvmArguments="-javaagent:/opt/k2-ic/K2-JavaAgent-1.0.0-jar-with-dependencies.jar" executableJarFileName="" disableJIT="false">+g' /opt/IBM/WebSphere/AppServer/profiles/AppSrv01/config/cells/DefaultCell01/nodes/DefaultNode01/servers/server1/server.xml



#start mysql
chmod 777 -R /var/lib/mysql/*
mysqld_safe &
sleep 5s
#Populate java application DemoApplication
cd /
mysql -u root --execute="create database test"
mysql -u root test < MySql-DemoApplication-db.sql

sed -i 's/8080/9080/g' attack.sh
sed -i 's/DemoApplication-0.0.1-SNAPSHOT/DemoApplicationMysql/g' attack.sh

#start server
sh /work/start_server.sh &
sleep 90s
#deploy application
sh /start_application.sh
