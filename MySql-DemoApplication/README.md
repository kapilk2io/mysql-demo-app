MySql-DemoApplication
=========================
- The repo contains files to obtain a test container with vulnerable java application running on Apache Tomcat and with possible MySQL injection attack.
- Types OF Calls This Application Contains :- 1. DB Calls.
- Types Of Attack This Application Contains :-1. SQL Injection.
## Steps to deploy container
- Clone the latest repository from GitHub:
    ```sh
    $ git clone https://github.com/k2io/QA-IntCode.git
    ```
- Create image without Java agent:  
    ```sh
    $ cd Vulnerable-Tests-Setup/Java/MySql-DemoApplication/
    $ docker build -t <imagename> -f Dockerfile_without_java_agent .
    ```
- Create image with Java agent on Tomcat Server:
    ```sh
    $ cd Vulnerable-Tests-Setup/Java/MySql-DemoApplication/
    $ docker build -t <imagename> .
    ```
- Create image with Java agent and application depoyed on Jetty server:
    ```sh
    $ cd Vulnerable-Tests-Setup/Java/MySql-DemoApplication/
    $ docker build -f Dockerfile_jetty -t <imagename> .
    ```
- To pull docker image from docker hub, use the following command.
    ```sh
    $ docker pull k2cyber/test_application:mysql-demo-app
    ```      
- Deploy container for Tomcat:
    ```sh
    $ docker run -itd -v /opt/k2-ic:/opt/k2-ic -e K2_OPTS=" -javaagent:/opt/k2-ic/K2-JavaAgent-1.0.0-jar-with-dependencies.jar" -p 8080:8080 --name mysql-demo-app k2cyber/test_application:mysql-demo-app
    ```
- Deploy container for Jetty:
    ```sh
    $ docker run -itd -v /opt/k2-ic:/opt/k2-ic -e K2_OPTS=" -javaagent:/opt/k2-ic/K2-JavaAgent-1.0.0-jar-with-dependencies.jar" -p <port-for-host>:8080 --name <containername> <imagename>
    ```


## Steps to perform injection:
1. Curl Command:

##### CASE-1

   ```sh
    $ curl -X GET \
    'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+%271%27%3D%271' \
    -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
    -H 'accept-encoding: gzip, deflate' \
    -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
    -H 'cache-control: no-cache' \
    -H 'connection: keep-alive' \
    -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
    -H 'upgrade-insecure-requests: 1' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
   ```

##### CASE-2(SQL Injection in Prepared Statemet)

   ```sh
    $  curl 'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/UserCheck2?user=test%27+or+%271%27%3D%271&password=wrong' \
    -H 'Connection: keep-alive' \
    -H 'Upgrade-Insecure-Requests: 1' \
    -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86  Safari/537.36' \
    -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' \
    -H 'Referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample2.jsp' \
    -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
    -H 'Cookie: JSESSIONID=991C2E8BADB5199E3E5B8D18F8E76D22' \
    --compressed
   ```

##### CASE-3 (String.valueOf)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/ValueOf?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache,no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-4 (append)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/PlusEquals?user=test%27%20or%201=%271%20--&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache,no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```
##### CASE-5 (if else different number of WS)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase1?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache,no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-6 (ifelse with same WS)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase3?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache,no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-7 (ifelse with unbalanced tree for string builder object)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase2?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache,no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-8 (if else with different String Builder Objects)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase4?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-9 (Delete)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/DELCase1?user=test%27+OR+%271%27%3D%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-10 (Update)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/UPDCase1?user=test%27%20or%20%271%27=%271&password=987654321' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache,no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-11 (Insert or Update)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/INSUPD?user=test%27+OR+%271%27%3D%271&password=123456789' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-12 (If statement with manipulation of single StringBuilder Object.)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase6?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-13 (If else statements with multiple stringbuilder Objects being manipulated depending upon the condition fulfilled)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase7?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```
##### CASE-14 (If else statements with multiple stringbuilder Objects being manipulated in either case.)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase8?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-15 (If statement with manipulation of multiple StringBuilder Objects.)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase10?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-16 (Multiple If-Else with single String Builder Object.)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase11?user=test&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-17 ( Multiple If-Else with multiple String Builder Object.)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase12?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-18 (Multilevel If-Else with with Multiple String Builder Object.)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase13?user=test%27%20or%20%271%27=%271&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
  ```

##### CASE-19 (Multilevel If-Else with with single String Builder Object.)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase9?user=test&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
   ```

##### CASE-20 (Complex if-elseif-else case with multiple StringBuilder objects)

  ```sh
   $ curl -X GET \
   'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/SBCase14?user=test&password=' \
   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
   -H 'accept-encoding: gzip, deflate' \
   -H 'accept-language: en-US,en;q=0.9,hi;q=0.8' \
   -H 'cache-control: no-cache' \
   -H 'connection: keep-alive' \
   -H 'referer: http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/sample1.jsp' \
   -H 'upgrade-insecure-requests: 1' \
   -H user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
   ```


2. wrk tool:
    - Update target ip and port in config.txt.
    - Run lua script using wrk:
        ```sh
        $ wrk -t10 -c10 -d10 -s DemoApplication.lua 'http://<ip>:<port-for-host>/DemoApplication-0.0.1-SNAPSHOT/UserCheck?user=test&password=1%27+OR+%271%27%3D%271'
        ```
    - **Note**:
        - t = Number of threads to use
        - c = Connections to keep open
        - d = Duration of test
