Overview
=========

##### Total Number of generated Events: 32
##### Total Number of Events labled "ProbableAttacks": 8
##### Total Number of Events labled "Attacks": 13
##### Total Number of Events labled "Pass": 11
##### K2 Intcode Agent Build ID: 189
##### Elements In FunctionList: 18
##### Number of ValidationRules: 17

Event
======
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "ca28562db51e2734f33934fe20ee2f94c724d27b1bd68ea2fb3efffc1a188c48",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@b1fdcff",
      "\"SELECT city, country, email, phone, firstname, lastname FROM  User where userid='test' AND password='1' OR '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@2bf023ed\""
    ],
    "className": "com.k2cybersecurity.demo.sql.UserCheck",
    "startTime": 1549519400747,
    "lineNumber": 128,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "SELECT"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549519400747,
    "numberOfWhiteSpaces": 14,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549519400756,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "67c8277834b881c9ce380660c1a32bf4db892561f7b74436e812e371205e4185",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@2e1a7cd3",
      "\"select * from User where userid='test' or '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@1018d0f6\""
    ],
    "className": "com.k2cybersecurity.demo.misc.ValueOf",
    "startTime": 1549536509997,
    "lineNumber": 65,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549536224233,
    "numberOfWhiteSpaces": 7,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549536510001,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "7c0f59a0e87884a753cdafe1e987ae4f1a5128434b895d3ea25e672e7a9cf31c",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@5a09019c",
      "\"select * from User where userid='test' or 1='1 ---root'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@7337e69\""
    ],
    "className": "com.k2cybersecurity.demo.misc.PlusEquals",
    "startTime": 1549519858750,
    "lineNumber": 64,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549519748170,
    "numberOfWhiteSpaces": 8,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549519858756,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "f5925ec0012600287a9d8ccf3f4425f368a48fe94a78f33d688d23e578f380d6",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@554aee74",
      "\"select userId, password, city from User where userid='test' or '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@152fb51d\""
    ],
    "className": "com.k2cybersecurity.demo.stringbuilder.Case1",
    "startTime": 1549519921404,
    "lineNumber": 77,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549519858755,
    "numberOfWhiteSpaces": 9,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549519921409,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "02646075368541b72875a0808ba04daa6c99b7bcd1e7508a5561dd32da02eb9c",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@7d8c3389",
      "\"select userId from User where userid='test' or '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@398423ca\""
    ],
    "className": "com.k2cybersecurity.demo.stringbuilder.Case3",
    "startTime": 1549519973241,
    "lineNumber": 77,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549519921408,
    "numberOfWhiteSpaces": 7,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549519973246,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "f39640f6b3ab55329102e432c3faf07b941ab21ab83678ec3ad0c3da16bd357b",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@cf79357",
      "\"select * from User where userid='test' or '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@34039624\""
    ],
    "className": "com.k2cybersecurity.demo.stringbuilder.Case2",
    "startTime": 1549536887287,
    "lineNumber": 73,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549536510001,
    "numberOfWhiteSpaces": 7,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549536887291,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "a30bd80788d64748731fef8c958d0debda632932449d721b9bbc850ef26717ad",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@503492b0",
      "\"select * from User where userid='test' or '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@6b3d61b2\""
    ],
    "className": "com.k2cybersecurity.demo.stringbuilder.Case5",
    "startTime": 1549537143495,
    "lineNumber": 75,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549536887290,
    "numberOfWhiteSpaces": 7,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549537143500,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "a36d1d98c1ddf1c6f4e1de5dbb18913d8cf78c5aa723ee018399e86dad23b3ca",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@2f0140eb",
      "\"delete from test where userid='test' OR '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@1181e3d5\""
    ],
    "className": "com.k2cybersecurity.demo.sql.DeleteCase1",
    "startTime": 1549520424624,
    "lineNumber": 67,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "delete"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549520424625,
    "numberOfWhiteSpaces": 6,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549520424630,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "b24a225e8a82b86a1d76ce96a426839452ba4f11ea98c8a1b97da2ecb75a15ca",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@a3bec69",
      "\"update test set password='987654321' where userid='test' or '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@56688b95\""
    ],
    "className": "com.k2cybersecurity.demo.sql.UpdateCase1",
    "startTime": 1549520539253,
    "lineNumber": 66,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "update"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549520539253,
    "numberOfWhiteSpaces": 7,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549520539257,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "f20075852f726acb2f68c88f97634b9481b47305c2a16b121acd8e578f4352ca",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@70a5db28",
      "\"update User set password='123456789' where userid='test' OR '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@7145f64\""
    ],
    "className": "com.k2cybersecurity.demo.sql.InsertOrUpdate",
    "startTime": 1549520605645,
    "lineNumber": 71,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "update"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549520605645,
    "numberOfWhiteSpaces": 7,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549520605648,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "5ec3992f410154322ae093bdfa874b71c640dac5fc5a9e0d1321c0ea2eab943c",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@70a5db28",
      "\"select userid from User where userid='test' OR '1'='1'\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@2004f65f\""
    ],
    "className": "com.k2cybersecurity.demo.sql.InsertOrUpdate",
    "startTime": 1549520605639,
    "lineNumber": 64,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549520539256,
    "numberOfWhiteSpaces": 7,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549520605645,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "08b1866ff008d418753c403fdf01c5d62480926a410528d9ad1c0aef4d4b2bb9",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@60fba8bf",
      "\"CALL buildTestTable()\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@5e565e57\""
    ],
    "className": "com.k2cybersecurity.demo.sql.DeleteCase1",
    "startTime": 1549520424620,
    "lineNumber": 60,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549520424622,
    "numberOfWhiteSpaces": 0,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549520424624,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "b67225fa680945b522eaae736be2c2d56d9dd684578b5e77d7eaeecf60282346",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@6e3c4ad0",
      "\"select @@session.tx_read_only\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@6b45de76\""
    ],
    "className": "com.k2cybersecurity.demo.sql.DeleteCase1",
    "startTime": 1549520424614,
    "lineNumber": 60,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549520356397,
    "numberOfWhiteSpaces": 1,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549520424623,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "03627a6c3c41203f5994bfd05ba2d0113e73a761172bcb66056dbd9e8a332069",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@1282abe4",
      "\"select @@session.tx_read_only\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@1ba59d85\""
    ],
    "className": "com.k2cybersecurity.demo.sql.DeleteCase1",
    "startTime": 1549520424623,
    "lineNumber": 67,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549520424623,
    "numberOfWhiteSpaces": 1,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549520424625,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```
```json
{
  "guid": "",
  "sha256": "",
  "eventId": "4ce7280b159831d9daa6605d3347fba842e087930a30f2c5770a99c28de11cc5",
  "version": "0.0.1",
  "jsonName": "intcode_event",
  "eventInfo": {
    "name": "processRequest",
    "type": null,
    "status": "VALIDATED",
    "arguments": [
      "com.mysql.jdbc.StatementImpl@352a60f3",
      "\"select @@session.tx_read_only\"",
      "\"WINDOWS-1252\"",
      "\"[Ljava.lang.Object;@32a2e5e9\""
    ],
    "className": "com.k2cybersecurity.demo.sql.InsertOrUpdate",
    "startTime": 1549520605644,
    "lineNumber": 71,
    "sourceMethod": "final com.mysql.jdbc.ResultSetInternalMethods com.mysql.jdbc.MysqlIO.sqlQueryDirect(com.mysql.jdbc.StatementImpl,java.lang.String,java.lang.String,com.mysql.jdbc.Buffer,int,int,int,boolean,java.lang.String,com.mysql.jdbc.Field[]) throws java.lang.Exception",
    "dbCommandList": [
      {
        "dbName": null,
        "tableNames": null,
        "commandName": "select"
      }
    ],
    "systemCallList": null,
    "validationBypass": false,
    "concernedImageIds": null,
    "receivedTimeStamp": 1549520605644,
    "numberOfWhiteSpaces": 1,
    "fleOpenAndCreateList": null,
    "vulnerabilityCaseType": "DB_COMMAND",
    "dynamicClassLoadingList": null
  },
  "eventType": "JAVA_EVENT",
  "timestamp": 1549520605646,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "applicationPid": 182,
  "applicationUUID": "af4e2d91-8f68-45db-91d7-2b69d3583282",
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  }
}
```

EventResult
============
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "ca28562db51e2734f33934fe20ee2f94c724d27b1bd68ea2fb3efffc1a188c48",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549519400757,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10001",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.sql.UserCheck",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549519400799,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549519400757,
  "validatorTimestamp": 1549519400799,
  "eventOccurenceTimestamp": 1549519400747,
  "validationServiceTimestamp": 1549519400757,
  "eventResultValidationEndTimestamp": 1549519400799,
  "eventResultValidationStartTimestamp": 1549519400758,
  "eventResultFetchForValidationTimestamp": 1549519400757
}
```
```json
{
  "guid": "",
  "result": "ProbableAttack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "67c8277834b881c9ce380660c1a32bf4db892561f7b74436e812e371205e4185",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549536510002,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "ProbableAttack",
    "detail": "List of DB Commands didn't match",
    "message": null,
    "confidenceLevel": "LOW"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.misc.ValueOf",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549536510027,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549536510002,
  "validatorTimestamp": 1549536510027,
  "eventOccurenceTimestamp": 1549536509997,
  "validationServiceTimestamp": 1549536510002,
  "eventResultValidationEndTimestamp": 1549536510027,
  "eventResultValidationStartTimestamp": 1549536510002,
  "eventResultFetchForValidationTimestamp": 1549536510002
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "7c0f59a0e87884a753cdafe1e987ae4f1a5128434b895d3ea25e672e7a9cf31c",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549519858756,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10015",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.misc.PlusEquals",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549519858782,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549519858756,
  "validatorTimestamp": 1549519858782,
  "eventOccurenceTimestamp": 1549519858750,
  "validationServiceTimestamp": 1549519858756,
  "eventResultValidationEndTimestamp": 1549519858782,
  "eventResultValidationStartTimestamp": 1549519858757,
  "eventResultFetchForValidationTimestamp": 1549519858756
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "f5925ec0012600287a9d8ccf3f4425f368a48fe94a78f33d688d23e578f380d6",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549519921410,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10007",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.stringbuilder.Case1",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549519921437,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549519921410,
  "validatorTimestamp": 1549519921437,
  "eventOccurenceTimestamp": 1549519921404,
  "validationServiceTimestamp": 1549519921410,
  "eventResultValidationEndTimestamp": 1549519921437,
  "eventResultValidationStartTimestamp": 1549519921410,
  "eventResultFetchForValidationTimestamp": 1549519921410
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "02646075368541b72875a0808ba04daa6c99b7bcd1e7508a5561dd32da02eb9c",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549519973246,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10004",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.stringbuilder.Case3",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549519973272,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549519973246,
  "validatorTimestamp": 1549519973272,
  "eventOccurenceTimestamp": 1549519973241,
  "validationServiceTimestamp": 1549519973246,
  "eventResultValidationEndTimestamp": 1549519973272,
  "eventResultValidationStartTimestamp": 1549519973247,
  "eventResultFetchForValidationTimestamp": 1549519973246
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "f39640f6b3ab55329102e432c3faf07b941ab21ab83678ec3ad0c3da16bd357b",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549536887292,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10013",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.stringbuilder.Case2",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549536887320,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549536887292,
  "validatorTimestamp": 1549536887320,
  "eventOccurenceTimestamp": 1549536887287,
  "validationServiceTimestamp": 1549536887292,
  "eventResultValidationEndTimestamp": 1549536887320,
  "eventResultValidationStartTimestamp": 1549536887292,
  "eventResultFetchForValidationTimestamp": 1549536887292
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "a30bd80788d64748731fef8c958d0debda632932449d721b9bbc850ef26717ad",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549537143501,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10017",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.stringbuilder.Case5",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549537143526,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549537143501,
  "validatorTimestamp": 1549537143526,
  "eventOccurenceTimestamp": 1549537143495,
  "validationServiceTimestamp": 1549537143501,
  "eventResultValidationEndTimestamp": 1549537143526,
  "eventResultValidationStartTimestamp": 1549537143501,
  "eventResultFetchForValidationTimestamp": 1549537143501
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "a36d1d98c1ddf1c6f4e1de5dbb18913d8cf78c5aa723ee018399e86dad23b3ca",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549520424632,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10016",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.sql.DeleteCase1",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549520424664,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549520424631,
  "validatorTimestamp": 1549520424664,
  "eventOccurenceTimestamp": 1549520424624,
  "validationServiceTimestamp": 1549520424632,
  "eventResultValidationEndTimestamp": 1549520424664,
  "eventResultValidationStartTimestamp": 1549520424632,
  "eventResultFetchForValidationTimestamp": 1549520424632
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "b24a225e8a82b86a1d76ce96a426839452ba4f11ea98c8a1b97da2ecb75a15ca",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549520539259,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10005",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.sql.UpdateCase1",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549520539291,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549520539258,
  "validatorTimestamp": 1549520539291,
  "eventOccurenceTimestamp": 1549520539253,
  "validationServiceTimestamp": 1549520539259,
  "eventResultValidationEndTimestamp": 1549520539291,
  "eventResultValidationStartTimestamp": 1549520539259,
  "eventResultFetchForValidationTimestamp": 1549520539259
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "f20075852f726acb2f68c88f97634b9481b47305c2a16b121acd8e578f4352ca",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549520605649,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10011",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.sql.InsertOrUpdate",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549520605679,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549520605649,
  "validatorTimestamp": 1549520605679,
  "eventOccurenceTimestamp": 1549520605645,
  "validationServiceTimestamp": 1549520605649,
  "eventResultValidationEndTimestamp": 1549520605679,
  "eventResultValidationStartTimestamp": 1549520605649,
  "eventResultFetchForValidationTimestamp": 1549520605649
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "5ec3992f410154322ae093bdfa874b71c640dac5fc5a9e0d1321c0ea2eab943c",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549520605646,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10012",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.sql.InsertOrUpdate",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549520605682,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549520605646,
  "validatorTimestamp": 1549520605682,
  "eventOccurenceTimestamp": 1549520605639,
  "validationServiceTimestamp": 1549520605646,
  "eventResultValidationEndTimestamp": 1549520605682,
  "eventResultValidationStartTimestamp": 1549520605647,
  "eventResultFetchForValidationTimestamp": 1549520605646
}
```
```json
{
  "guid": "",
  "result": "Pass",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "08b1866ff008d418753c403fdf01c5d62480926a410528d9ad1c0aef4d4b2bb9",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549520424625,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Pass",
    "detail": "DB Command list matches with list from rule id 10014",
    "message": null,
    "confidenceLevel": "LOW"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.sql.DeleteCase1",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549520424668,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549520424624,
  "validatorTimestamp": 1549520424668,
  "eventOccurenceTimestamp": 1549520424620,
  "validationServiceTimestamp": 1549520424625,
  "eventResultValidationEndTimestamp": 1549520424668,
  "eventResultValidationStartTimestamp": 1549520424625,
  "eventResultFetchForValidationTimestamp": 1549520424625
}
```
```json
{
  "guid": "",
  "result": "ProbableAttack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "b67225fa680945b522eaae736be2c2d56d9dd684578b5e77d7eaeecf60282346",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549520424623,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "ProbableAttack",
    "detail": "List of DB Commands didn't match",
    "message": null,
    "confidenceLevel": "LOW"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.sql.DeleteCase1",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549520424657,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549520424623,
  "validatorTimestamp": 1549520424657,
  "eventOccurenceTimestamp": 1549520424614,
  "validationServiceTimestamp": 1549520424623,
  "eventResultValidationEndTimestamp": 1549520424657,
  "eventResultValidationStartTimestamp": 1549520424624,
  "eventResultFetchForValidationTimestamp": 1549520424623
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "03627a6c3c41203f5994bfd05ba2d0113e73a761172bcb66056dbd9e8a332069",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549520424626,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10016",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.sql.DeleteCase1",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549520424672,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549520424626,
  "validatorTimestamp": 1549520424672,
  "eventOccurenceTimestamp": 1549520424623,
  "validationServiceTimestamp": 1549520424626,
  "eventResultValidationEndTimestamp": 1549520424672,
  "eventResultValidationStartTimestamp": 1549520424627,
  "eventResultFetchForValidationTimestamp": 1549520424626
}
```
```json
{
  "guid": "",
  "result": "Attack",
  "sha256": "",
  "toolId": {
    "validator": "201711102011_UTC_0da29af"
  },
  "eventId": "4ce7280b159831d9daa6605d3347fba842e087930a30f2c5770a99c28de11cc5",
  "version": "0.0.1",
  "jsonName": "intcode_event_result",
  "timestamp": 1549520605647,
  "customerId": 502,
  "k2ICToolId": "201902051202_UTC_4edeaf0",
  "eventDetail": {
    "type": "Attack",
    "detail": "List of DB Commands didn't match with rule-id 10011",
    "message": "SQL Injection Attack",
    "confidenceLevel": "VALID"
  },
  "finalResult": "Pass",
  "topModuleName": "com.k2cybersecurity.demo.sql.InsertOrUpdate",
  "dis2dhReadTime": 0,
  "dis2dhWriteTime": 0,
  "resultTimestamp": 1549520605674,
  "sourceIdentifier": {
    "nodeId": "926e3f31-194f-11e9-9bdd-577f9ec104e3",
    "nodeIp": "172.31.4.19",
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395",
    "containerId": "384201121e4bb3564805f7d00ee5dab6db658660bc07e43a86a71845f0e45e42",
    "containerName": "ic-mysql-demo-app"
  },
  "consumerTimestamp": 1549520605647,
  "validatorTimestamp": 1549520605674,
  "eventOccurenceTimestamp": 1549520605644,
  "validationServiceTimestamp": 1549520605647,
  "eventResultValidationEndTimestamp": 1549520605674,
  "eventResultValidationStartTimestamp": 1549520605647,
  "eventResultFetchForValidationTimestamp": 1549520605647
}
```

ValidationRule
==============
```json
{
    "numberOfWS": 12, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10001, 
    "timestamp": 1549454879604, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.sql.UserCheck", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "SELECT"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 128, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "2f29873dacccfc60551f11c416e67b7d4107649b03e407d23be4f3c08aced633", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 3, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10002, 
    "timestamp": 1549454879609, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.sql.UserCheck", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "SELECT"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 110, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "33baedd846f9c7c292f7632da0c6e6ffb17bb5e026b49c3cdab87b85c89acde5", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": -1, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10003, 
    "timestamp": 1549454879612, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": false, 
    "fileName": "com.k2cybersecurity.demo.sql.UserCheck2", 
    "activeStatus": true, 
    "dbCommands": [], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "LOW", 
    "lineNumber": 110, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "22bdfce6cbe54369dd43100f8cf54184050cfbac1269e4284d4a401f0d3eb0bb", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 5, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10004, 
    "timestamp": 1549454879615, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.stringbuilder.Case3", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "select"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 77, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "a9be8b9703ae29f5d8d46dc3d305153a61fde519a76c229e06f059e362393da2", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 5, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10005, 
    "timestamp": 1549454879618, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.sql.UpdateCase1", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "update"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 66, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "dd047a48ce99884efaa40146c5cadb45bb537aee1070b9b6930d7257bbda2458", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 5, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10006, 
    "timestamp": 1549454879621, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.stringbuilder.Case1", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "select"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 77, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "1176e1738ef117d81b3471e5d6337ea5d3ccc545ad0b26de0cf56f9789a506f6", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 7, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10007, 
    "timestamp": 1549454879625, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.stringbuilder.Case1", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "select"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 77, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "24ec89f22e344af07ab21bd87c323dd62175e6486cdc5932bea965e7650f94b8", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 5, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10008, 
    "timestamp": 1549454879628, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.sql.UserCheck2", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "SELECT"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 107, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "5f69b2dcd9f0edf7b720f6181e102749bd675b4cbbaacd9256f9886246cc8043", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": -1, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10009, 
    "timestamp": 1549454879631, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": false, 
    "fileName": "com.k2cybersecurity.demo.stringbuilder.Case4", 
    "activeStatus": true, 
    "dbCommands": [], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "LOW", 
    "lineNumber": 74, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "813ac47a284f406dc9a23640da3351bb6de0228eac9dce1825726fe8bf241fa0", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": -1, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10010, 
    "timestamp": 1549454879633, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": false, 
    "fileName": "com.k2cybersecurity.demo.misc.ValueOf", 
    "activeStatus": true, 
    "dbCommands": [], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "LOW", 
    "lineNumber": 65, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "c8192d9546fbca13039e64e4d08cf00816955a228528bdde9470007705dc9967", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 10, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10011, 
    "timestamp": 1549454879636, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.sql.InsertOrUpdate", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "INSERT"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 71, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "894d3ea42f9500815ac4c9b00cd6d65662e4176ed8648a3d0c114a2dd1fc30b5", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 5, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10012, 
    "timestamp": 1549454879639, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.sql.InsertOrUpdate", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "select"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 64, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "2004ab889afd6feed130085efd4a9c11b1fad6e27c7e333b7da42a3e67d77c44", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 5, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10013, 
    "timestamp": 1549454879642, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.stringbuilder.Case2", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "select"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 73, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "e67e947aa609ad2c5dc074e08fcaa53191917e55eb076a29481000baf3a97b3d", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": -1, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10014, 
    "timestamp": 1549454879645, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": false, 
    "fileName": "com.k2cybersecurity.demo.sql.DeleteCase1", 
    "activeStatus": true, 
    "dbCommands": [], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "LOW", 
    "lineNumber": 60, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "bc0f9eb2be2edd5f14049681d8d68065a2eaa0834d40b4b287c0a7fc5ae44dc5", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 5, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10015, 
    "timestamp": 1549454879647, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.misc.PlusEquals", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "select"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 64, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "76b23f23b0a7dbf1fe07b7f5de7f79c53125ef8143f5f9b4be4f731f4c6ac9e5", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 4, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10016, 
    "timestamp": 1549454879651, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.sql.DeleteCase1", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "delete"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 67, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "23296f543b1ac936f7ba6a4ce740440b4620f01b9648cb6eeed9023f20a33bb4", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```
```json
{
    "numberOfWS": 5, 
    "ruleType": "static", 
    "guid": "", 
    "ruleId": 10017, 
    "timestamp": 1549454879657, 
    "jsonName": "intcode_validation_rule", 
    "imageId": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
    "resolvedCommands": true, 
    "fileName": "com.k2cybersecurity.demo.stringbuilder.Case5", 
    "activeStatus": true, 
    "dbCommands": [
        {
            "dbName": null, 
            "tableNames": null, 
            "commandName": "select"
        }
    ], 
    "version": "0.0.1", 
    "hitCount": 0, 
    "confidenceLevel": "VALID", 
    "lineNumber": 75, 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "sha256": "cb0b4942d1690c2c94290854d0ed73840e6db77e0bc3923e45c116fc61d7c6bd", 
    "customerId": 502, 
    "vulnerabilityCaseType": "DB_COMMAND"
}
```

ScriptMap
==========
```json
{
    "guid": "", 
    "timestamp": 1549454879660, 
    "jsonName": "intcode_script_map", 
    "processStage": "InitialProcess", 
    "version": "0.0.1", 
    "scriptTarget": {
        "ip": "", 
        "type": "image", 
        "id": "db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395", 
        "name": "[k2cyber/test_application:mysql-demo-app]"
    }, 
    "hostIp": "172.31.4.19", 
    "k2ICToolId": "201902051202_UTC_4edeaf0", 
    "fileInfo": {
        "processingTime": 932, 
        "path": "/root/.k2adp/imagefs/db2074a99d6b3a414866c06725f18d3c7e5ce2b63b9f16a55dc0e6fcd433c395/etc/apache-tomcat-8.5.34/webapps/DemoApplication-0.0.1-SNAPSHOT.war", 
        "sha256": "e1f9e2fc17bf097078b9b6df4d518cad402f8ddc7e1b2b01b76a48d6e2b75497", 
        "type": "Java"
    }, 
    "sha256": "e1f9e2fc17bf097078b9b6df4d518cad402f8ddc7e1b2b01b76a48d6e2b75497", 
    "functionList": [
        {
            "numberOfWhiteSpaces": 12, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "SELECT"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.sql.UserCheck", 
            "arguments": [
                {
                    "type": null, 
                    "name": "query", 
                    "value": "SELECT city, country, email, phone, firstname, lastname FROM  User where userid='<user>' AND password='<pwd>'"
                }
            ], 
            "lineNumber": 128, 
            "functionCaller": "com.k2cybersecurity.demo.sql.UserCheck.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 3, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "SELECT"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.sql.UserCheck", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "SELECT userId FROM  User"
                }
            ], 
            "lineNumber": 110, 
            "functionCaller": "com.k2cybersecurity.demo.sql.UserCheck.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "name": "java.sql.PreparedStatement.executeQuery()", 
            "dbCommandList": [], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.sql.UserCheck2", 
            "arguments": [], 
            "lineNumber": 110, 
            "functionCaller": "com.k2cybersecurity.demo.sql.UserCheck2.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 5, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "select"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.stringbuilder.Case3", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "select * from User where userid='<user>'"
                }
            ], 
            "lineNumber": 77, 
            "functionCaller": "com.k2cybersecurity.demo.stringbuilder.Case3.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 5, 
            "name": "java.sql.Statement.execute(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "update"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.sql.UpdateCase1", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "update test set password='<pwd>' where userid='<user>'"
                }
            ], 
            "lineNumber": 66, 
            "functionCaller": "com.k2cybersecurity.demo.sql.UpdateCase1.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 5, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "select"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.stringbuilder.Case1", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "select * from User where userid='<user>'"
                }
            ], 
            "lineNumber": 77, 
            "functionCaller": "com.k2cybersecurity.demo.stringbuilder.Case1.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 7, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "select"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.stringbuilder.Case1", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "select userId, password, city from User where userid='<user>'"
                }
            ], 
            "lineNumber": 77, 
            "functionCaller": "com.k2cybersecurity.demo.stringbuilder.Case1.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 5, 
            "name": "java.sql.Connection.prepareStatement(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "SELECT"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.sql.UserCheck2", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "SELECT * FROM User where userid=?"
                }
            ], 
            "lineNumber": 107, 
            "functionCaller": "com.k2cybersecurity.demo.sql.UserCheck2.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": -1, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.stringbuilder.Case4", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "<ENDIF>'"
                }
            ], 
            "lineNumber": 74, 
            "functionCaller": "com.k2cybersecurity.demo.stringbuilder.Case4.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": -1, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.misc.ValueOf", 
            "arguments": [
                {
                    "type": null, 
                    "name": "str1", 
                    "value": null
                }
            ], 
            "lineNumber": 65, 
            "functionCaller": "com.k2cybersecurity.demo.misc.ValueOf.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 10, 
            "name": "java.sql.Statement.execute(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "INSERT"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.sql.InsertOrUpdate", 
            "arguments": [
                {
                    "type": null, 
                    "name": "query", 
                    "value": "INSERT INTO `User` values('<user>', '<pwd>', null, null, null, null, null, null)"
                }
            ], 
            "lineNumber": 71, 
            "functionCaller": "com.k2cybersecurity.demo.sql.InsertOrUpdate.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 5, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "select"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.sql.InsertOrUpdate", 
            "arguments": [
                {
                    "type": null, 
                    "name": "str1", 
                    "value": "select userid from User where userid='<user>'"
                }
            ], 
            "lineNumber": 64, 
            "functionCaller": "com.k2cybersecurity.demo.sql.InsertOrUpdate.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 5, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "select"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.stringbuilder.Case3", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "select userId from User where userid='<user>'"
                }
            ], 
            "lineNumber": 77, 
            "functionCaller": "com.k2cybersecurity.demo.stringbuilder.Case3.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 5, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "select"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.stringbuilder.Case2", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "select * from User where userid='<sb>'"
                }
            ], 
            "lineNumber": 73, 
            "functionCaller": "com.k2cybersecurity.demo.stringbuilder.Case2.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": -1, 
            "name": "java.sql.Statement.execute(java.lang.String)", 
            "dbCommandList": [], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.sql.DeleteCase1", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "CALL buildTestTable()"
                }
            ], 
            "lineNumber": 60, 
            "functionCaller": "com.k2cybersecurity.demo.sql.DeleteCase1.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 5, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "select"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.misc.PlusEquals", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "select * from User where userid='<user>-root'"
                }
            ], 
            "lineNumber": 64, 
            "functionCaller": "com.k2cybersecurity.demo.misc.PlusEquals.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 4, 
            "name": "java.sql.Statement.execute(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "delete"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.sql.DeleteCase1", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "delete from test where userid='<user>'"
                }
            ], 
            "lineNumber": 67, 
            "functionCaller": "com.k2cybersecurity.demo.sql.DeleteCase1.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }, 
        {
            "numberOfWhiteSpaces": 5, 
            "name": "java.sql.Statement.executeQuery(java.lang.String)", 
            "dbCommandList": [
                {
                    "dbName": null, 
                    "tableNames": null, 
                    "commandName": "select"
                }
            ], 
            "isUnresolved": false, 
            "className": "com.k2cybersecurity.demo.stringbuilder.Case5", 
            "arguments": [
                {
                    "type": null, 
                    "name": "__CONST__", 
                    "value": "select * from User where userid='"
                }
            ], 
            "lineNumber": 75, 
            "functionCaller": "com.k2cybersecurity.demo.stringbuilder.Case5.processRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)", 
            "type": "DBCommand"
        }
    ], 
    "customerId": 502, 
    "hostId": "926e3f31-194f-11e9-9bdd-577f9ec104e3"
}
```
