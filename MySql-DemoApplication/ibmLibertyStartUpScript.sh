#!/bin/bash

#start mysql
mysqld_safe &
echo "Sleeping 30 sec for MySql startup..."
sleep 30s
#Populate java application DemoApplication
cd /
mysql -u root --execute="create database test"
mysql -u root test < MySql-DemoApplication-db.sql

sed -i 's/8080/9080/g' attack.sh

export JVM_ARGS=$K2_OPTS

./etc/wlp/bin/server start
