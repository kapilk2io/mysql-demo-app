#!/bin/bash

#start mysql
mysqld_safe &
echo "Sleeping 30 sec for MySql startup..."
sleep 30s

#Populate java application DemoApplication
cd /
mysql -u root --execute="create database test"
mysql -u root test < MySql-DemoApplication-db.sql

#start jetty
export JAVA_OPTS=$K2_OPTS

cd $JETTY_HOME /
java $JAVA_OPTS -jar start.jar &
cd /
